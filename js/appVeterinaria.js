document.addEventListener("DOMContentLoaded", function () {
    const btnCargarRazas = document.getElementById("btnCargarRazas");
    const slctRaza = document.getElementById("slctRaza");
    const imgPerro = document.getElementById("imgPerro");
    const btnCargarIMG = document.getElementById("btnCargarIMG");

    btnCargarRazas.addEventListener("click", function () {
        cargarRazas();
    });

    btnCargarIMG.addEventListener("click", function () {
        cargarIMG();
    });

    function cargarRazas() {
        axios.get("https://dog.ceo/api/breeds/list")
            .then(response => {
                const seleccionRaza = slctRaza;
                seleccionRaza.innerHTML = ""; // Limpiar opciones existentes
                const razas = response.data.message;
                razas.forEach(raza => {
                    const opcionActual = document.createElement("option");
                    opcionActual.textContent = raza;
                    seleccionRaza.appendChild(opcionActual);
                });
            })
            .catch(error => {
                console.error("Ocurrió un error...", error);
            });
    }

    function cargarIMG() {
        const selectedBreed = slctRaza.value;
        axios.get(`https://dog.ceo/api/breed/${selectedBreed}/images/random`)
            .then(response => {
                const url = response.data.message;
                imgPerro.src = url;
            })
            .catch(error => {
                console.error("Ocurrió un error...", error);
            });
    }
});
