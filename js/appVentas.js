document.addEventListener("DOMContentLoaded", function () {
    
    const btnBuscar = document.getElementById("btnBuscar");
    const btnLimpiar = document.getElementById("btnLimpiar");
    const id = document.getElementById("inputID");
    const lista = document.getElementById("lista");

    function buscarUsuario() {
        const userId = id.value.trim();

        if (userId === "") {
            alert("Inserte una ID...");
            return;
        }

        axios.get(`https://jsonplaceholder.typicode.com/users/${userId}`)
            .then(response => {
                const vendedor = response.data;
                lista.innerHTML = generarHTMLUsuario(vendedor);
            })
            .catch(error => {
                manejarError("No existe dicha ID...");
            });
    }

    function generarHTMLUsuario(vendedor) {
        return `
            <tr>
                <td>${vendedor.id}</td>
                <td>${vendedor.name}</td>
                <td>${vendedor.username}</td>
                <td>${vendedor.email}</td>
                <td> Calle: ${vendedor.address.street} <br> Número: ${vendedor.address.suite} <br> Ciudad: ${vendedor.address.city}</td>
            </tr>`;
    }

    function manejarError(mensaje) {
        console.error("ERROR EN PETICIÓN:", mensaje);
        alert(mensaje);
    }

    btnBuscar.addEventListener("click", buscarUsuario);

    btnLimpiar.addEventListener("click", function () {
        id.value = "";
        lista.innerHTML = "";
    });
});
